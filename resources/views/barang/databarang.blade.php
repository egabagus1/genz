@extends('layout/template')

@section('judul_halaman', 'Data Barang')

@section('konten')
    <div class="container p-4">
        <div class="card">
            <div class="card-header border-transparent">
                <h3 class="card-title">Pesanan Terbaru</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
                <div class="table-responsive">
                    <table class="table m-0">
                        <thead>
                            <tr>
                                <th>Timestamp</th>
                                <th>Item</th>
                                <th>Qty</th>
                                <th>Customer</th>
                                <th>Total Harga</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><a href="pages/examples/invoice.html">19/9/2023 19.00</a></td>
                                <td>Modular Ecuador Dining Table</td>
                                <td>100</td>
                                <td>Otazen Factory</td>
                                <td>IDR 29.459.000</td>
                                <td>
                                    <div class="btn btn-sm btn-primary">Detail</div>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="pages/examples/invoice.html">19/9/2023 19.00</a></td>
                                <td>Modular Ecuador Dining Table</td>
                                <td>100</td>
                                <td>Otazen Factory</td>
                                <td>IDR 29.459.000</td>
                                <td>
                                    <div class="btn btn-sm btn-primary">Detail</div>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="pages/examples/invoice.html">19/9/2023 19.00</a></td>
                                <td>Modular Ecuador Dining Table</td>
                                <td>100</td>
                                <td>Otazen Factory</td>
                                <td>IDR 29.459.000</td>
                                <td>
                                    <div class="btn btn-sm btn-primary">Detail</div>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="pages/examples/invoice.html">19/9/2023 19.00</a></td>
                                <td>Modular Ecuador Dining Table</td>
                                <td>100</td>
                                <td>Otazen Factory</td>
                                <td>IDR 29.459.000</td>
                                <td>
                                    <div class="btn btn-sm btn-primary">Detail</div>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="pages/examples/invoice.html">19/9/2023 19.00</a></td>
                                <td>Modular Ecuador Dining Table</td>
                                <td>100</td>
                                <td>Otazen Factory</td>
                                <td>IDR 29.459.000</td>
                                <td>
                                    <div class="btn btn-sm btn-primary">Detail</div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.card-body -->
            <div class="card-footer clearfix">
                <a href="javascript:void(0)" class="btn btn-sm btn-primary float-right">View All Orders</a>
            </div>
            <!-- /.card-footer -->
        </div>
    </div>
@endsection
