@extends('layout/template')

@section('judul_halaman', 'Pesanan')

@section('konten')
    <div class="container p-4">
        <div id="tab">
            <nav>
                <a href="#" class="active" data-id='1'>Tab 1</a>
                <a href="#" data-id='2'>Tab 2</a>
                <a href="#" data-id='3'>Tab 3</a>
            </nav>

            <div class="tab-content active" data-content='1'>
                Isi Tab 1
            </div>
            <div class="tab-content" data-content='2'>
                Isi Tab 2
            </div>
            <div class="tab-content" data-content='3'>
                Isi Tab 3
            </div>
        </div>
    </div>

    <script src="{{ 'js/function.js' }}"></script>
@endsection
