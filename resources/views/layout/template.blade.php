<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('judul_halaman') | GENZ</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
        crossorigin="anonymous">
    <!-- Theme style -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/3.2.0/css/adminlte.min.css"
        crossorigin="anonymous">
    <script src="https://use.fontawesome.com/c74e39ff47.js"></script>
    <link rel="stylesheet" href="dist/css/style.css" type="text/css">

    <style>
        .info-box {
            background-color: #4C4C6D;
            color: #fff;
            padding: 15px 10px;
            box-shadow: none;
        }

        .nav-link>p {
            font-size: 10pt;
        }

        .main-footer {
            font-size: 10pt;
        }

        .card {
            box-shadow: none;
        }

        .btn-primary {
            background-color: #1B9C85;
        }

        .btn-primary:hover {
            background-color: #0d6454
        }
    </style>
</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed "
    style="font-family: 'poppins';">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light" style="background-color: #fff;">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item d-none d-sm-inline-block pl-4">
                    @yield('judul_halaman')
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">

                <!-- Messages Dropdown Menu -->

                <li class="nav-item">
                    <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                        <i class="fas fa-expand-arrows-alt"></i>
                    </a>
                </li>
                <div class="nav-item user-panel d-flex">
                    <div class="image">
                        <i class="fa-solid fa-user"></i>
                    </div>
                    <div class="info">
                        <a href="#" class="d-block">Alexander Pierce</a>
                    </div>
                </div>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4" style="background-color: #4C4C6D;">
            <!-- Brand Logo -->
            <a href="#" class="brand-link" style="background-color: #4C4C6D">
                <span class="brand-text font-weight-light p-4">
                    <img src="https://i.ibb.co/KNhWvb4/Logo.png" style="width: 150px;">
                </span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">

                <!-- Sidebar Menu -->
                <nav class="mt-4">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                        data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                        <li class="nav-item pt-2 pb-2">
                            <a href="/" class="nav-link">
                                <i class="fas fa-regular fa-house nav-icon"></i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <span style="border: 1px solid rgba(175, 175, 175, 0.185)"></span>
                        <li class="nav-item pt-2 pb-2">
                            <a href="pesanan" class="nav-link">
                                <i class="fa-solid fa-bag-shopping nav-icon"></i>
                                <p>Pesanan</p>
                            </a>
                        </li>
                        <span style="border: 1px solid rgba(175, 175, 175, 0.185)"></span>
                        <li class="nav-item pt-2 pb-2">
                            <a href="#" class="nav-link">
                                <i class="fa-solid fa-tags nav-icon"></i>
                                <p>Diskon/Voucher</p>
                            </a>
                        </li>
                        <span style="border: 1px solid rgba(175, 175, 175, 0.185)"></span>
                        <li class="nav-item pt-2 pb-2">
                            <a href="barang" class="nav-link">
                                <i class="fa-solid fa-box nav-icon"></i>
                                <p>Data Barang</p>
                            </a>
                        </li>
                        <span style="border: 1px solid rgba(175, 175, 175, 0.185)"></span>
                        <li class="nav-item pt-2 pb-2">
                            <a href="#" class="nav-link">
                                <i class="fa-solid fa-chart-simple nav-icon"></i>
                                <p>Laporan</p>
                            </a>
                        </li>
                        <span style="border: 1px solid rgba(175, 175, 175, 0.185)"></span>
                        <li class="nav-item pt-2 pb-2">
                            <a href="#" class="nav-link">
                                <i class="fa-solid fa-right-from-bracket nav-icon"></i>
                                <p>Logout</p>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="background-color: #e7e7e7; min-height: 1000px;">
            <!-- Content Header (Page header) -->
            @yield('konten')
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
                <b>Version</b> 1.0.0
            </div>
            Made with <i class="fa-solid fa-heart" style="color: red;"></i> by <strong> GenZ team</strong> | All rights
            reserved.
        </footer>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.3.0/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/3.2.0/js/adminlte.min.js"></script>

    {{-- Chart JS --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/4.3.0/chart.min.js"></script>


</body>

</html>
